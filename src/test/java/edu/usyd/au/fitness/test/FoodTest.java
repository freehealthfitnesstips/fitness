package edu.usyd.au.fitness.test;

import java.util.List;





import edu.usyd.au.fitness.domain.Food;
import edu.usyd.au.fitness.domain.User;
import edu.usyd.au.fitness.domain.UserWeight;
import edu.usyd.au.fitness.domain.Weight;
import edu.usyd.au.fitness.domain.recommend.RecommendedActivity;
import edu.usyd.au.fitness.service.CalendarService;
import edu.usyd.au.fitness.service.FitnessService;
import junit.framework.TestCase;

public class FoodTest extends TestCase{
	private User user;
	private Food food;
	private FitnessService fitnessService;
	protected void setUp() throws Exception
	{
		food = new Food();
	}
	
	public void testSetAndGetFoodName()
	{
		String testFoodname = "aFoodname";
		assertNull(food.getFoodName());
		food.setFoodName(testFoodname);
		assertEquals(testFoodname, food.getFoodName());
	}
	
	public void testSetAndGetCalories()
	{
		double testCalories = 100.00;
		assertEquals(0,0,0);
		food.setCals(testCalories);
		assertEquals(testCalories, food.getCals(), 0);
	}
	
	public void testSetAndGetFat()
	{
		double testFat = 100.00;
		assertEquals(0,0,0);
		food.setFat(testFat);
		assertEquals(testFat, food.getFat(), 0);
	}
	
	public void testSetAndGetAmount()
	{
		double testAmount = 100.00;
		assertEquals(0,0,0);
		food.setAmount(testAmount);
		assertEquals(testAmount, food.getAmount(), 0);
	}
	
	public void queryWeightById() {
		
    	Weight weights =  fitnessService.queryWeightById(1);
    	
        assertEquals("fitnessService", true, weights.getId() > 0);
    }
	
	public void queryUserWeightByUserId()
	{		
		List<UserWeight> userweight = fitnessService.queryUserWeightByUserId(1);
	    	
	    assertEquals("queryUserWeightByUserId", true, userweight.size() > 0);	    
	}
}
