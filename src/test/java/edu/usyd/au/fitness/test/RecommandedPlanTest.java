package edu.usyd.au.fitness.test;

import java.util.List;

import edu.usyd.au.fitness.domain.User;
import edu.usyd.au.fitness.domain.recommend.RecommendedActivity;
import edu.usyd.au.fitness.service.CalendarService;
import junit.framework.TestCase;

public class RecommandedPlanTest extends TestCase {
	
	CalendarService calendarService;

    protected void setUp() throws Exception {
    	calendarService = new CalendarService();
    }

    public void recommandedPlan() {
    	List<RecommendedActivity> acts = calendarService.queryRecommendedActivity();
    	
        assertEquals("recommandedPlan", true, acts.size() > 0);
    }

    public void testJsonOfActivities() {
    	User user = new User();
    	user.setId(1);
    	String json = calendarService.getJsonOfActivities(user);
        
    	assertEquals("testJsonOfActivities", true, json.length() > 0);
    }
  
}