//var pgArgs = new Array("MIT", "MITM", "GD")
//var bgArgs = new Array("BCS", "BIT", "BE")
var pgArgs = {
				"Master of Information Technology": [
					{ "Computer Networks major":"Computer Networks major"},
					{ "Database Management Systems major":"Database Management Systems major"},
					{ "Software Engineering major":"Software Engineering major"},
					{ "Computer Science major":"Computer Science major"},
					{ "Telecommunications Engineering major":"Telecommunications Engineering major"},
					{ "Business Information Systems major":"Business Information Systems major"},
					{ "Project Management major":"Project Management major"},
					{ "Health Informatics major":"Health Informatics major"}
				],
				"Master of Information Technology Management": [
					{ "MITM":"Master of Information Technology Management"}
				],
				"Graduate Diploma in Information Technology": [
							{ "Graduate Diploma in Information Technology":"Graduate Diploma in Information Technology"}
						],
				"Graduate Certificate in Information Technology": [
					{ "Graduate Certificate in Information Technology":"Graduate Certificate in Information Technology"}
				]
			}
var bgArgs = {
				"BCST, BCST (Adv) and BIT degrees": [
					{ "BCST":"BCST"},
					{ "BCST(Adv)":"BCST(Adv)"},
					{ "BIT":"BIT"}
				],
				"BIT Combined degrees": [
					{ "BIT/BCom":"BIT/BCom"},
					{ "BIT/BSc":"BIT/BSc"},
					{ "BIT/BMedSc":"BIT/BMedSc"},
					{ "BIT/BA":"BIT/BA"},
					{ "BIT/LLB":"BIT/LLB"}
				],
				"BSc, BSc (Adv and combined degrees": [
					{ "Computer Science (CS)":"Computer Science (CS)"},
					{ "Information Systems (IS)":"Information Systems (IS)"},
					{ "Mathematics":"Mathematics"}
				]
			}
$(document).ready(function() {
	$("#level").change(function() {
		$("#degree").empty()
		$("#major").empty()
		$("#degree").append("<option value=\"null\">" + "---- Select A Degree -----" + "</option>")
		$("#major").append("<option value=\"null\">" + "---- Select A Major -----" + "</option>")
		if($("#level").val() == "PostGraduate - coursework"){
			$.each(pgArgs, function(id, name) {
				$("#degree").append("<option value=\"" + id + "\">" + id + "</option>")
			})
		}
		else{
			$.each(bgArgs, function(id, name) {
				$("#degree").append("<option value=\"" + id + "\">" + id + "</option>")
			})
		}
		
	})
	$("#degree").change(function() {
		$("#major").empty()
		$("#major").append("<option value=\"null\">" + "----- Select A Major -----" + "</option>")
		$.each(pgArgs, function(id, name) {
			if($("#degree").val() == id){
				$.each(pgArgs[id], function(id, name) {
					$.each(name, function(id, name) {
						$("#major").append("<option value=\"" + id + "\">" + name + "</option>")
					})
				})
			}
		})
		$.each(bgArgs, function(id, name) {
			if($("#degree").val() == id){
				$.each(bgArgs[id], function(id, name) {
					$.each(name, function(id, name) {
						$("#major").append("<option value=\"" + id + "\">" + name + "</option>")
					})
				})
			}
		})
	})
})

