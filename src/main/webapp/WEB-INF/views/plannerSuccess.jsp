<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
        <div class="mainbar">
          <div class="article">
          <h3>Your fitness evaluation: </h3>
              <ol>
              	<li>
              	  <label for="standardWeight">According to your height, the standard weight should be around <font color="blue"><c:out value="${model.planner.standardWeight}" />KG.</font></label>
                </li>
                <li>
                  <label for="weightRange">Your current weight location: </label>
                  <img src="http://chart.apis.google.com/chart?cht=bhs&chs=600x50&chd=t:${model.planner.percentage}&chxt=x,y&chxl=0:|0|Underweight|Light%20weight|Healthy%20weight|Overweight|Obses|135KG|1:||" alt="Sample chart" />
                      
                </li>
                <li>
                  <label for="caloriePerWeek">You need to consume <font color="blue"><c:out value="${model.planner.caloriePerWeek}" /></font> kCal per week.</label>           
                </li>
<!--                 <li> -->
<%--                 <form:form modelAttribute="editAccount" method="POST" action="editAccount" id="editForm"> --%>
<%--                   <input type="hidden" name="email" value="${model.user.email}"/> --%>
<%--                   <input type="hidden" name="password" value="${model.user.password}"/> --%>
<%--                   <input type="hidden" name="userName" value="${model.user.userName}"/> --%>
<%--                   <input value="   Edit    " type="submit" name="imageField" id="imageField" src="${pageContext.request.contextPath}/resources/images/submit.gif" class="send" /> --%>
<!--                   <div class="clr"></div> -->
<%--                 </form:form> --%>
                  
<%--                 <form:form modelAttribute="submitAccount" method="POST" action="saveProfile" id="submitForm"> --%>
<%--                   <input type="hidden" name="email" value="${model.user.email}"/> --%>
<%--                   <input type="hidden" name="password" value="${model.user.password}"/> --%>
<%--                   <input type="hidden" name="userName" value="${model.user.userName}"/> --%>
<%--                   <input value="Submmit" type="submit" name="imageField" id="imageField" src="${pageContext.request.contextPath}/resources/images/submit.gif" class="send" /> --%>
<!--                   <div class="clr"></div> -->
<%--                 </form:form> --%>
<!--                 </li> -->
              </ol>          
          
          
          
          
          
          
          
          
          
          
          
          
<%--           <c:out value="${model.planner.standardWeight}"/> --%>
<%--           <c:out value="${model.planner.percentage}"/> --%>
<%--           <c:out value="${model.planner.caloriePerWeek}"/> --%>
          </div>
          <div class="pagenavi"><span class="pages"> </span></div>
        </div>
        <%@ include file="/WEB-INF/views/menu.jsp"%>
        <script type="text/javascript">$("#PLANNER").addClass("active")</script>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
