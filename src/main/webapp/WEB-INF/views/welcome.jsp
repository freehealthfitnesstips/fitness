<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
        <br>
        <br>
        <br>
        <br>
          <h2 style="text-align:center">Welcome to Fitness!</h2>
            <div style="text-align:center" class="article">
<%--               <form:form modelAttribute="loginInput" method="POST" action="setLoginInput" id="loginForm"> --%>
              <form:form method="POST" action="setLoginInput" id="loginForm">
                <ol>
              	  <li>
                    <label for="email">User Email (Login ID)</label>
                    <input id="email" name="email" type="text" class="text" />
                  </li>
                  <li>
                    <label for="password">Password</label>
                    <input id="password" name="password" type="password" class="text" />
                  </li>
                  <li>
<!--                     <label for="password">Password</label> -->
<!--                     <input id="password" name="password" type="password" class="text" /> -->
                    <font color="red"><c:out value="${model.error}" /></font>
<%--                     <font color="red"><c:out value="${model.input.password}" /></font> --%>
                  </li>
                  
                  <li>
                    <input value=" Sign In " type="submit" name="imageField" id="imageField" src="${pageContext.request.contextPath}/resources/images/submit.gif" class="send" />
                    <div class="clr"></div>
                  </li>
                </ol>
              </form:form>

              <form:form modelAttribute="register" method="GET" action="register" id="registerForm">
                <ol>
                  <li>
                    <input value="Sign Up" type="submit" name="imageField" id="imageField" src="${pageContext.request.contextPath}/resources/images/submit.gif" class="send" />
                    <div class="clr"></div>
                  </li>
                </ol>
              </form:form>
              <br>
                <ol>
                  <li>
                    <a href="${pageContext.request.contextPath}/reset">Forgot password ?</a>
                    <div class="clr"></div>
                  </li>
                </ol>
              
              
              
              
              
              
            </div>
<!--         <div class="mainbar"> -->
<!--           <div class="article"> -->
<!--           	<h2>Welcome to Fitness!</h2> -->
<!--             <div class="clr"></div> -->
<!--             <p class="post-data"> -->
<!--             	We believe - and medical studies prove - that the best way to lose weight and keep it off forever is to simply keep track of the foods you eat. Gimmicky machines and fad diets don't work, so we designed a free website and mobile apps that make calorie counting and food tracking easy.  -->
<!--             </p> -->
<!--            	<h3>MyFitnessPal really works!</h3> -->
<!--            	<p class="post-data"> -->
<!--     MFP has been instrumental to my weight loss. I've tried countless times to lose weight but nothing worked. Anything restrictive and I would just gain the weight right back. But MyFitnessPal really works. MFP makes counting calories SO easy. I log onto my iPhone app every day and I'm proud to say I haven't missed a single day of logging in. The community aspect on this site is amazing and I've made some really great friends throughout this journey. I can't wait to continue losing weight with MFP!  -->
<!-- Juliette Willson (julwills)  -->
<!--             </p> -->
<!--             <div class="clr"></div> -->
<!--           </div> -->
<!--           <div class="pagenavi"><span class="pages"> </span></div> -->
<!--         </div> -->
<%--         <%@ include file="/WEB-INF/views/menu.jsp"%> --%>
<!--         <script type="text/javascript">$("#HOME").addClass("active")</script> -->
<!--         <div class="clr"></div> -->
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
