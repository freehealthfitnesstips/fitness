<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
       <% Object user = session.getAttribute("user");%>
        <div class="mainbar">
<!--           <div class="article"> -->
<!--             <div class="clr"></div> -->
<%--             <c:if test="${isAddedSuc}"> --%>
<!--           		<class="post-data"> Congratulations! Register Complete. Let us start to set your planner!</p> -->
<%--             </c:if> --%>
<!--             <div class="clr"></div> -->
<!--           </div> -->
          <div class="article">
<%--             <form:form modelAttribute="reminder" method="POST" action="setReminder" id="reminderForm"> --%>
                  <label for="email">Email: <font color="blue"><c:out value="${user.email}"/></font></label>
            <form:form modelAttribute="reminder" method="POST" action="setReminder" id="reminderForm">
              <ol>        
                <li>
                  <p>Please select time interval by sending reminder email. </p>
<%--                   <label for="email">Email: <font color="blue"><c:out value="${model.user.email}"/></font></label> --%>
<%--                   <input type="hidden" name="email" value="${model.user.email}"/> --%>
                  <label for="timeInterval">Sending Frequency Setting: </label>
                  <select name="timeInterval">
                    <option value="">No reminder</option>                    
                    <option value="1">Every day</option>
	                <option value="2">Every 2 days</option>
                    <option value="4">Every 4 days</option>
	              </select>
                </li>  
                <li>
                  <input value="Submmit" type="submit" name="imageField" id="imageField" src="${pageContext.request.contextPath}/resources/images/submit.gif" class="send" />
                  <div class="clr"></div>
                </li>                    
              </ol>
            </form:form>
          </div>
          <div class="pagenavi"><span class="pages"> </span></div>
        </div>
        <%@ include file="/WEB-INF/views/menu.jsp"%>
        <script type="text/javascript">$("#REMINDER").addClass("active")</script>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
