<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/calendar.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery-ui.custom.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/fullcalendar.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/gcal.js'/>"></script>
	<link href="<c:url value='/resources/css/fullcalendar.css'/>" rel="stylesheet" type="text/css" />
	<link href="<c:url value='/resources/css/jquery-ui.min.css'/>" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function() {
	var date = new Date("2013 10 8");
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();
	
	$('#calendar').fullCalendar({
		
		// US Holidays
		eventClick: function(event, jsEvent, view) {
			if(event.warmUp == null){
				$("#dietDetail").show();
				$("#breakfast").html("Breakfast: " + event.breakfast)
				$("#lunch").html("Lunch: " + event.lunch)
				$("#snack").html("Snack: " + event.snack)
				$("#dinner").html("Dinner: " + event.dinner)
				$("#dietDetail").css("top",jsEvent.pageY);
				$("#dietDetail").css("left",jsEvent.pageX);
			}
			else{
				$("#actDetail").show();
				$("#warmUp").html("WarmUp: " + event.warmUp)
				$("#exercise").html("Exercise: " + event.exercise)
				$("#coolDown").html("CoolDown: " + event.coolDown)
				$("#totalExe").html("TotalExecise: " + event.totalTime)
				$("#actDetail").css("top",jsEvent.pageY);
				$("#actDetail").css("left",jsEvent.pageX);
			}
		},
		eventMouseout: function(event, jsEvent, view) {
			  $("#actDetail").hide();
			  $("#dietDetail").hide();
		},
		events: ${model.events}
	});
	
})

</script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
        <div class="mainbar">
          <div id="calendar" class="calendar" >
          <div id="actDetail" class="fc-event fc-event-hori fc-event-start fc-event-end" style="top: 35.4px; left: 173px; position: absolute; width: 220px; height: 112px;z-index: 1000;display:none;">
			<table class="pop_up_table">
				<tr>
					<td id="warmUp" style="padding: 5px;height: 15px;width: 200px;"></td>
				</tr>
				<tr>
					<td id="exercise" style="padding: 5px;height: 15px;width: 200px;"></td>
				</tr>
				<tr>
					<td id="coolDown" style="padding: 5px;height: 15px;width: 200px;"></td>
				</tr>
				<tr>
					<td id="totalExe" style="padding: 5px;height: 15px;width: 200px;"></td>
				</tr>
			</table>
		  </div>
		  <div id="dietDetail" class="fc-event fc-event-hori fc-event-start fc-event-end" style="top: 35.4px; left: 173px; position: absolute; width: 220px; height: 162px;z-index: 1000;display:none;">
			<table class="pop_up_table">
				<tr>
					<td id="breakfast" style="padding: 5px;height: 15px;width: 200px;"></td>
				</tr>
				<tr>
					<td id="lunch" style="padding: 5px;height: 15px;width: 200px;"></td>
				</tr>
				<tr>
					<td id="snack" style="padding: 5px;height: 15px;width: 200px;"></td>
				</tr>
				<tr>
					<td id="dinner" style="padding: 5px;height: 15px;width: 200px;"></td>
				</tr>
			</table>
		  </div>
          </div>
          <div class="pagenavi"><span class="pages"> </span></div>
        </div>
        <%@ include file="/WEB-INF/views/menu.jsp"%>
        <script type="text/javascript">$("#CALENDAR").addClass("active")</script>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
