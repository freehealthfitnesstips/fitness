<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>
    <div class="content">
      <div class="content_bg">
        <div class="mainbar">
          <div class="article">
            <div class="clr"></div>
          </div>
          <div class="article">
          
          <label for="user">UserName: <font color="blue"><c:out value="${user.userName}"/></font></label><br>
          <p>The history of your evaluation is below. </p><br>
          <table id="mytable" cellspacing="0">
            	<tr>
            		<th>DATE</th>
            		<th>LEVEL</th>
            		<th>SCORE</th>
            	</tr>
            	<c:forEach items="${model.results}" var="r">
				<tr>
					<td class=""><fmt:formatDate type="date" pattern="dd/MM/yyyy" value="${r.date}" /></td>
            		<td class="">${r.level}</td>
            		<td class="">${r.score}</td>
            		
            	</tr>
				</c:forEach>
            </table>
          </div>
          <div class="pagenavi"><span class="pages"> </span></div>
        </div>
        <%@ include file="/WEB-INF/views/menu.jsp"%>
        <script type="text/javascript">$("#HISTORY").addClass("active");</script>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
