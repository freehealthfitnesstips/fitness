<title>Fitness</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link href="<c:url value='/resources/css/style.css'/>" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<c:url value='/resources/js/jquery-1.9.0.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/script.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/cufon-yui.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/arial.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/cuf_run.js'/>"></script>
