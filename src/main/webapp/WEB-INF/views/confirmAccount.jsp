<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.validate.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/validator.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/enquire.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    </div>

    <div class="content">
    
      <div class="content_bg">
        <br>
        <br>
        <br>
        <br>
        <% Object user = session.getAttribute("user");%>
<!--         <div class="mainbar"> -->
          <h3 style="text-align:center">Your account information</h3>
<!--             <div style="text-align:center" class="article"> -->
<!--           <div style="text-align:center" class="article"> -->
          <div style="text-align:center" class="article">
<!--           <h3>Your account information: </h3> -->
              <ol>
              	<li>
              	  <label for="email">Email: <font color="blue"><c:out value="${user.email}" /></font></label>
                </li>
                <li>
                  <label for="password">Password: <font color="blue"><c:out value="${user.password}" /></font></label>    
                </li>
                <li>
                  <label for="name">Name: <font color="blue"><c:out value="${user.userName}" /></font></label>           
                </li>
                <li>
<%--                 <form:form modelAttribute="editAccount" method="POST" action="editAccount" id="editForm"> --%>
                <form:form method="POST" action="editAccount" id="editForm">
<%--                   <input type="hidden" name="email" value="${user.email}"/> --%>
                  <input type="hidden" name="password" value="${user.password}"/>
                  <input type="hidden" name="password" value="${user.confirmPassword}"/>
<%--                   <input type="hidden" name="userName" value="${user.userName}"/> --%>
                  <input value="   Edit    " type="submit" name="imageField" id="imageField" src="${pageContext.request.contextPath}/resources/images/submit.gif" class="send" />
                  <div class="clr"></div>
                </form:form>
                  
<%--                 <form:form modelAttribute="submitAccount" method="POST" action="saveProfile" id="submitForm"> --%>
                <form:form method="POST" action="saveAccount" id="submitForm">
                  <input type="hidden" name="email" value="${user.email}"/>
                  <input type="hidden" name="password" value="${user.password}"/>
                  <input type="hidden" name="userName" value="${user.userName}"/>
                  <input value="Submmit" type="submit" name="imageField" id="imageField" src="${pageContext.request.contextPath}/resources/images/submit.gif" class="send" />
                  <div class="clr"></div>
                </form:form>
                </li>
              </ol>
          </div>
<!--           <div class="pagenavi"><span class="pages"> </span></div> -->
<!--         </div> -->
<%--         <%@ include file="/WEB-INF/views/menu.jsp"%> --%>
<!--         <script type="text/javascript">$("#REGISTER").addClass("active")</script> -->
<!--         <div class="clr"></div> -->
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>
</html>
