<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<link href="<c:url value='/resources/css/reveal.css'/>" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<c:url value='/resources/js/jquery-1.4.4.min.js'/>"></script>
</head>
<body>
<div class="main">
  <div class="main_resize">
    <div class="header">
    	<%@ include file="/WEB-INF/views/head.jsp"%>
    	<div class="menu_nav">
		  	<ul class="logInfo_ul">
		    	<li><a href="<c:url value="/j_spring_security_logout" />" >Logout</a></li>
		    </ul>
		</div>
    </div>
    <div class="content">
      <div class="content_bg">
      	<div class="clr"></div>
        <div class="mainbar" style="width:920px;">
        	<h3>
            	<span>Fitness Recommendation Plan Management</span>
            </h3>
            <table id="mytable" cellspacing="0">
            	<tr>
            		<th>NAME</th>
            		<th>WARMUP</th>
            		<th>EXERCISE</th>
            		<th>COOLDOWN</th>
            		<th>RANGE OF DATE</th>
            		<th>SEQUENCY</th>
            		<th>TOTALTIME</th>
            		<th>Operation</th>
            	</tr>
            	<c:forEach items="${model.acts}" var="act">
            	<tr>
					<th class="spec">${act.title}</td>
            		<td>${act.warmUp}</td>
            		<td>${act.exercise}</td>
            		<td>${act.coolDown}</td>
            		<td>${act.rangeOfDate}</td>
            		<td>${act.sequency}</td>
            		<td>${act.totalTime}</td>
            		<td><a href="#"  data-reveal-id="act_div_popup_${act.id}">Change</a></td>
            	</tr>
            	</c:forEach>
				<c:if test="${model.acts.size() eq 0}">
					<tr><td colspan="8" align="center">No FAQs results for the Filer</td></tr>
				</c:if>
				<c:if test="${model.acts.size() > 0}">
					<tr><td colspan="8" align="left"> Totel Records: ${model.acts.size()}</td></tr>
				</c:if>
            </table>
            <div class="t10">
            	<a href="#"  data-reveal-id="act_div_popup_${act.id}">Add A New Activity</a>
            </div>
            <div class="clr"></div>
        </div>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="fbg">
    <div class="fbg_resize">
      <div class="clr"></div>
    </div>
  </div>
</div>
<div class="footer">
  <div class="footer_resize">
    <p class="lf">&copy;  University of Sydney - School of Information technology.</p>
    <div class="clr"></div>
  </div>
</div>

<c:forEach items="${model.acts}" var="act">
<form:form modelAttribute="recommendedActivity" method="POST" action="admin/save" id="activityForm">
<div id="act_div_popup_${act.id}" class="reveal-modal">
	<div class="article t15">
		<h4><span>Activity Ref: ${act.id}</span></h4>
		<div class="clr"></div>
		<table id="mytable_enquiry_popup" width="100%" cellspacing="0">
          	<tr>
          		<th>NAME</th>
          		<td><input name="title" type="text" value="${act.title}" class="text" /></td>
          	</tr>
          	<tr>
          		<th>WARMUP</th>
          		<td><input name="warmUp" type="text" value="${act.warmUp}" class="text" /></td>
          	</tr>
          	<tr>
          		<th>EXERCISE</th>
          		<td><input name="exercise" type="text" value="${act.exercise}" class="text" /></td>
          	</tr>
          	<tr>
          		<th>COOLDOWN</th>
          		<td><input name="coolDown" type="text" value="${act.coolDown}" class="text" /></td>
          	</tr>
          	<tr>
          		<th>RANGE OF DATE</th>
          		<td><input name="rangeOfDate" type="text" value="${act.rangeOfDate}" class="text" /></td>
          	</tr>
          	<tr>
          		<th>SEQUENCY</th>
          		<td><input name="sequency" type="text" value="${act.sequency}" class="text" /></td>
          	</tr>
          	<tr>
				<th>TOTALTIME</th>
          		<td><input name="totalTime" type="text" value="${act.totalTime}" class="text" /></td>
          	</tr>
          	<tr>
          		<th>Enable</th>
          		<td>
          			<span class="filter_span"><input type="radio" class="rd_enquiryDate" name="enable" value="true" checked="checked" /> Yes</span>
          			<span class="filter_span"><input type="radio" class="rd_enquiryDate" name="enable" value="false" /> No</span>
          		</td>
          	</tr>
		</table>
		<input type="hidden" name="id" value="${act.id}" />
		<div class="t15 ct"><input type="submit" class="submitBt" name="" id="" value="Submit" /></div>
	</div>
	<a class="close-reveal-modal">&#215;</a>
</div>
</form:form>
</c:forEach>
<form:form modelAttribute="recommendedActivity" method="POST" action="admin/add" id="activityForm">
<div id="act_div_popup_${act.id}" class="reveal-modal">
	<div class="article t15">
		<h4><span>Activity Ref: ${act.id}</span></h4>
		<div class="clr"></div>
		<table id="mytable_enquiry_popup" width="100%" cellspacing="0">
          	<tr>
          		<th>NAME</th>
          		<td><input name="title" type="text" value="${act.title}" class="text" /></td>
          	</tr>
          	<tr>
          		<th>WARMUP</th>
          		<td><input name="warmUp" type="text" value="${act.warmUp}" class="text" /></td>
          	</tr>
          	<tr>
          		<th>EXERCISE</th>
          		<td><input name="exercise" type="text" value="${act.exercise}" class="text" /></td>
          	</tr>
          	<tr>
          		<th>COOLDOWN</th>
          		<td><input name="coolDown" type="text" value="${act.coolDown}" class="text" /></td>
          	</tr>
          	<tr>
          		<th>RANGE OF DATE</th>
          		<td><input name="rangeOfDate" type="text" value="${act.rangeOfDate}" class="text" /></td>
          	</tr>
          	<tr>
          		<th>SEQUENCY</th>
          		<td><input name="sequency" type="text" value="${act.sequency}" class="text" /></td>
          	</tr>
          	<tr>
				<th>TOTALTIME</th>
          		<td><input name="totalTime" type="text" value="${act.totalTime}" class="text" /></td>
          	</tr>
          	<tr>
          		<th>Enable</th>
          		<td>
          			<span class="filter_span"><input type="radio" class="rd_enquiryDate" name="enable" value="true" checked="checked" /> Yes</span>
          			<span class="filter_span"><input type="radio" class="rd_enquiryDate" name="enable" value="false" /> No</span>
          		</td>
          	</tr>
		</table>
		<div class="t15 ct"><input type="submit" class="submitBt" name="" id="" value="Submit" /></div>
	</div>
	<a class="close-reveal-modal">&#215;</a>
</div>
</form:form>
<script type="text/javascript" src="<c:url value='/resources/js/jquery.reveal.js'/>"></script>
</html>
