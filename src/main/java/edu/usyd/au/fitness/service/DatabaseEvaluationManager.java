package edu.usyd.au.fitness.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.usyd.au.fitness.domain.Evaluation;
import edu.usyd.au.fitness.domain.User;

@Service(value="evaluationManager")
@Transactional
public class DatabaseEvaluationManager implements EvaluationManager {
	
	/**
	 * 
	 */
	private SessionFactory sessionFactory;
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Evaluation> getEvaluations() {
		// TODO Auto-generated method stub
		String sql="FROM EVALUATION";
//		sql+=" where USER_NAME=lhe";
//		sql+=" order by id DATE ASC";
		return this.sessionFactory.getCurrentSession().
				createQuery(sql).list();
	}

	@Override
	public Evaluation getEvaluationById(long id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Evaluation evaluation = (Evaluation) currentSession.get(Evaluation.class, id);
		return evaluation;
		}

	@Override
	public void addEvaluation(Evaluation evaluation) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(evaluation);
		
	}
}
