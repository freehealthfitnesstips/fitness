package edu.usyd.au.fitness.service;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

//import edu.usyd.au.fitness.domain.Activity;
//import edu.usyd.au.fitness.domain.Food;



import edu.usyd.au.fitness.domain.Planner;
import edu.usyd.au.fitness.domain.User;


@Service(value="PlannerService")
@Transactional
public class PlannerService {

	private SessionFactory sessionFactory;
//	private int id;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public Planner addPlanner(Planner planner) {
		
		String gender = planner.getGender();
		Double standardWeight, percentage, caloriePerWeek;
		standardWeight = 0.0;
		
		if (gender.equals("male"))
		{
			standardWeight = planner.getHeight() - 105;
			
		}
		
		if (gender.equals("female"))
		{
			standardWeight = planner.getHeight() - 100;		
		}
		
		percentage = planner.getCurrentWeight() / 1.35;
		
		caloriePerWeek = (planner.getCurrentWeight() - planner.getGoalWeight()) / planner.getDuration() * 7.7;
		
		planner.setStandardWeight(standardWeight);
		planner.setPercentage(percentage);
		planner.setCaloriePerWeek(caloriePerWeek);
		
		Session session = this.sessionFactory.getCurrentSession();
		session.save(planner);
		session.flush();
		
		return planner;
		
	}
	
	public Planner updatePlanner(Planner planner) {
		
		String gender = planner.getGender();
		Double standardWeight, percentage, caloriePerWeek;
		standardWeight = 0.0;
		
		if (gender.equals("male"))
		{
			standardWeight = planner.getHeight() - 105;
			
		}
		
		if (gender.equals("female"))
		{
			standardWeight = planner.getHeight() - 100;		
		}
		
		percentage = planner.getCurrentWeight() / 1.35;
		
		caloriePerWeek = (planner.getCurrentWeight() - planner.getGoalWeight()) / planner.getDuration() * 7.7;
		
		planner.setStandardWeight(standardWeight);
		planner.setPercentage(percentage);
		planner.setCaloriePerWeek(caloriePerWeek);


		String sql = "from Planner p";
		sql += " where p.email = '"+planner.getEmail()+"'";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		List<Planner> planners =  query.list();
		
		planner.setId(planners.get(0).getId());
		
		Session session = this.sessionFactory.getCurrentSession();
//		User user = (User) currentSession.get(User.class, id);
		session.merge(planner);
		session.flush();
		
		return planner;
		
	}
	
	
	
	
	
	
	
	
	
	
	
//	public Object calculation(Planner planner)
//	{
//		String gender = planner.getGender();
//		Double standardWeight, percentage, caloriePerWeek;
//		
//		if (gender.equals("male"))
//		{
//			standardWeight = planner.getHeight() - 105;
//			
//		}
//		
//		if (gender.equals("female"))
//		{
//			standardWeight = planner.getHeight() - 100;		
//		}
//		
//		percentage = planner.getCurrentWeight() / 1.3;
//		
//		caloriePerWeek = (planner.getCurrentWeight() - planner.getGoalWeight()) / planner.getDuration() * 7.7;
//		
//		
//		
//		
//		return ;
//	}
	
//	public void addActivity(Activity activity) {
//		Session session = this.sessionFactory.getCurrentSession();
//		activity.setCreatedDate(new Date());
//		session.save(activity);
//		session.flush();
//		
//	}
	
//	public List<Food> queryFoods() {
//		String sql = "from Food";
//		sql += " order by id DESC";
//		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
//		
//		List<Food> foods = query.list();
//		
//		return foods;
//	}
	
//	public List<Food> queryActivities() {
//		String sql = "from Activity";
//		sql += " order by id DESC";
//		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
//		
//		List<Food> activities = query.list();
//		
//		return activities;
//	}
	
	public String checkPlanner(Planner planner) {
		
//		String gender = planner.getGender();
//		Double standardWeight, percentage, caloriePerWeek;
//		standardWeight = 0.0;
//		
//		if (gender.equals("male"))
//		{
//			standardWeight = planner.getHeight() - 105;
//			
//		}
//		
//		if (gender.equals("female"))
//		{
//			standardWeight = planner.getHeight() - 100;		
//		}
//		
//		percentage = planner.getCurrentWeight() / 1.35;
//		
//		caloriePerWeek = (planner.getCurrentWeight() - planner.getGoalWeight()) / planner.getDuration() * 7.7;
//		
//		planner.setStandardWeight(standardWeight);
//		planner.setPercentage(percentage);
//		planner.setCaloriePerWeek(caloriePerWeek);
		
		String info = "";


		String sql = "from Planner p";
		sql += " where p.email = '"+planner.getEmail()+"'";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		List<Planner> planners = query.list();

		if (planners.size() != 0)
		{
			info = "exist";
//			planner.setId(planners.get(0).getId());			
			return info;
		}
		else
		{
			info = "new";
			return info;

		}
		
		
	}
	
//	public Planner getExactPlanner()
//	{
//		
//		Session currentSession = this.sessionFactory.getCurrentSession();
//		Planner planner = (Planner) currentSession.get(Planner.class, id);
//		
//		return planner;
//	}
	
}









