package edu.usyd.au.fitness.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.usyd.au.fitness.domain.Evaluation;
import edu.usyd.au.fitness.domain.Result;

@Service(value="resultManager")
@Transactional
public class DatabaseResultManager implements ResultManager {
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	@Override
	public void addResult(Result result) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(result);
	}
	@Override
	public Result getResultById(long id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Result result = (Result) currentSession.get(Result.class, id);
		return result;
	}

	@Override
	public List<Result> getResults(String userName) {
		// TODO Auto-generated method stub
		List<Result> results=null;
		List<Result> temp=null;
		
		String sql="FROM Result ";		
		sql+=" order by DATE ASC";
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		results=query.list();
		temp=query.list();
		if(results!=null)
		{
			for (Result result : results) {
               if(result.getUserName().compareTo(userName)!=0)
               {
            	   temp.remove(result);
               }
            }
		}
		return temp;
	}
	
//	@Override
//	public void addProduct(Product product) {
//		this.sessionFactory.getCurrentSession().save(product);
//	}
//	
//	@Override
//	public Product getProductById(long id) {
//		Session currentSession = this.sessionFactory.getCurrentSession();
//		Product product = (Product) currentSession.get(Product.class, id);
//		return product;
//	}
//	
//	@Override
//	public void updateProduct(Product product) {
//		Session currentSession = this.sessionFactory.getCurrentSession();
//		currentSession.merge(product);
//	}
//	
//	
//	@Override
//	public void deleteProduct(long id) {
//		Session currentSession = this.sessionFactory.getCurrentSession();
//		Product product = (Product) currentSession.get(Product.class, id);
//		currentSession.delete(product);
//	}
//
//	@Override
//	public void increasePrice(int percentage) {
//		Session currentSession = this.sessionFactory.getCurrentSession();
//		List<Product> products = currentSession.createQuery("FROM Product").list();
//		
//		if (products != null) {
//            for (Product product : products) {
//                double newPrice = product.getPrice().doubleValue() * 
//                                    (100 + percentage)/100;
//                product.setPrice(newPrice);
//                currentSession.save(product);
//            }
//        }
//	}

	

}
