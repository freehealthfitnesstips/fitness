package edu.usyd.au.fitness.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.usyd.au.fitness.domain.Award;

@Service(value="awardManager")
@Transactional
public class DatabaseAwardManager implements AwardManager {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Award> getAwards() {
		// TODO Auto-generated method stub
		String sql="FROM Award";
		sql+=" order by Credit DESC";
		Session currentSession = this.sessionFactory.getCurrentSession();
		 List<Award> Arank=this.sessionFactory.getCurrentSession().
					createQuery(sql).list();
		 int i=0;
		 if (Arank != null) {
	            for (Award award : Arank) {
	            	i++;
	                award.setRank(i);
	                currentSession.save(award);
	            }
	        }
		return Arank;
	}
	@Override
	public void addAward(Award award) {
		// TODO Auto-generated method stub
		System.out.println(award.getUserName());
		this.sessionFactory.getCurrentSession().save(award);
	}
	
	@SuppressWarnings("null")
	@Override
	public void updateAward(String userName,int credit) {
		
		
		Award award=getAwardByUser(userName);
		if (award != null) {
			//award=awards.get(0);
            award.setCredit(award.getCredit()+credit);
            if(award.getCredit()%25==0)
            {
            	award.setBronze(award.getBronze()+1);
            	if(award.getBronze()%5==0)
            	{
            		award.setSilver(award.getSilver()+1);
            		award.setBronze(0);
            		if(award.getSilver()%5==0)
            		{
            			award.setGold(award.getGold()+1);
            			award.setSilver(0);
            		}
            	}
            }
        }
		else
		{
			award= new Award();
			award.setUserName(userName);
			award.setCredit(credit);
		}
		this.sessionFactory.getCurrentSession().save(award);
	}

	public Award getAwardByUser(String user) {
		
		List<Award> awards=null;
		Award temp=null;
		
		String sql="FROM Award ";	
		Query query = this.sessionFactory.getCurrentSession().createQuery(sql);
		awards=query.list();
		//temp=query.list();
		if(awards!=null)
		{
			for (Award award : awards) {
               if(award.getUserName().compareTo(user)==0)
               {
            	   temp=award;
               }
            }
		}
		return temp;
	}

}
