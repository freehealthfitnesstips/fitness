package edu.usyd.au.fitness.service;

import java.io.Serializable;
import java.util.List;

import edu.usyd.au.fitness.domain.Award;
import edu.usyd.au.fitness.domain.Evaluation;
import edu.usyd.au.fitness.domain.Result;

public interface AwardManager extends Serializable{
    
    public List<Award> getAwards();
    
    public void addAward(Award award);
    public void updateAward(String userName,int credit) ;
    public Award getAwardByUser(String user);

//    
//    public void deleteProduct(long id);
    
}