package edu.usyd.au.fitness.service;

import java.io.BufferedInputStream;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.usyd.au.fitness.domain.User;

public class EmailService {
	
	private static final Logger logger = LoggerFactory.getLogger(EmailService.class);
	Properties props;
	Session session;
	Message message;
	String username = "fitness.system.team@gmail.com";
	String password = "lzdsjlzdsj";
	Authentication auth = null;
	
	public EmailService() {
		auth = SecurityContextHolder.getContext().getAuthentication();
		/*InputStream inp;
		try {
			inp = new BufferedInputStream (new FileInputStream("resources/database.properties"));   
			props.load(inp);
			username = props.getProperty("email.username");
			password = props.getProperty("email.password");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
 
		props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		session = Session.getInstance(props,
				new javax.mail.Authenticator() {
				  protected PasswordAuthentication getPasswordAuthentication() {
					  return new PasswordAuthentication(username, password);
				  }
				});
		
		message = new MimeMessage(session);
		try {
			message.setFrom(new InternetAddress("fitness.system.team@gmail.com"));
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
//	public void sendEmailToStudent(User enquiry) {
//		
//		try {
//			message.setRecipients(Message.RecipientType.TO,
//			   InternetAddress.parse("fantasy_envoy@hotmail.com"));
//			message.setSubject("Your enquiry have been received.");
//			message.setText("Dear " + enquiry.getFirstName() + ","
//			   + "\n\n Thanks. Your enquire have been submited. Our advisers will contact you in two working days."
//			   + "\n\n In order to trace further enquiry, please keep the reference Id: " + enquiry.getId()
//			   + "\n\n The enquiry you asked is :" + enquiry.getQuestion()
//			   + "\n\n You also could check out someone's email that might help to solove your question from :  http://sydney.edu.au/engineering/it/current_students/undergrad/"
//			   + "\n\n Yours sincerely,"
//			   + "\n\n SIT Enquiry System"
//			   + "\n\n Please do not reply. To confirm this is a genuine email sent by the Enquiry System SIT.");
//
//			Transport.send(message);
//
//			logger.info("It have done to send confirm email to " + enquiry.getSid());
//
//	   } catch (MessagingException e) {
//		   throw new RuntimeException(e);
//	   }
//	}
	
	public void accountNotification(User user) 
	{
//		String toEmail = getStudyLevel(user);
		try 
		{
			message.setRecipients(Message.RecipientType.TO,
			   InternetAddress.parse(user.getEmail()));
			message.setSubject("Fitness account notification");
			message.setText("Dear " + user.getUserName() + ","
					   + "\n\n Congratulations! Your fitness account has been created. Thanks you for using our fitness websit."
					   + "\n\n\n\n\n\n\n Kinder Regards,"
					   + "\n\n Fitness Team"
					   + "\n\n Please do not reply. To confirm this is a genuine email sent by the Fitness System SIT.");
			Transport.send(message);

//			logger.info("It have done to send email to " + toEmail);
		} 
		catch (MessagingException e) 
		{
			throw new RuntimeException(e);
		}
	}
	
	public void updateNotification(User user) 
	{
//		String toEmail = getStudyLevel(user);
		try 
		{
			message.setRecipients(Message.RecipientType.TO,
			   InternetAddress.parse(user.getEmail()));
			message.setSubject("Fitness account change notification");
			message.setText("Dear " + user.getUserName() + ","
					   + "\n\n Your fitness account password has been updated."
					   + "\n\n Your new password is " + user.getPassword()
					   + "\n\n\n\n\n\n\n Kinder Regards,"
					   + "\n\n Fitness Team"
					   + "\n\n Please do not reply. To confirm this is a genuine email sent by the Fitness System SIT.");
			Transport.send(message);

//			logger.info("It have done to send email to " + toEmail);
		} 
		catch (MessagingException e) 
		{
			throw new RuntimeException(e);
		}
	}
	
	public void resetNotification(User user) 
	{
//		String toEmail = getStudyLevel(user);
		try 
		{
			message.setRecipients(Message.RecipientType.TO,
			   InternetAddress.parse(user.getEmail()));
			message.setSubject("Fitness password reset notification");
			message.setText("Dear " + user.getUserName() + ","
					   + "\n\n Your fitness account password is " + user.getPassword()
					   + "\n\n\n\n\n\n\n Kinder Regards,"
					   + "\n\n Fitness Team"
					   + "\n\n Please do not reply. To confirm this is a genuine email sent by the Fitness System SIT.");
			Transport.send(message);

//			logger.info("It have done to send email to " + toEmail);
		} 
		catch (MessagingException e) 
		{
			throw new RuntimeException(e);
		}
	}
	
	
	
//	public void replyEmailToStudent(Enquiry enquiry) {
//		
//		try {
//			message.setRecipients(Message.RecipientType.TO,
//			   InternetAddress.parse(enquiry.getEmail() + "@uni.sydney.edu.au"));
//			message.setSubject("A reply of your Enquiry (Ref:" + enquiry.getId() + ").");
//			message.setText("Dear " + enquiry.getFirstName() + ","
//			   + "\n\n " + enquiry.getRepliedComment() 
//			   + "\n\n In order to trace further enquiry, please keep the reference Id: " + enquiry.getId()
//			   + "\n\n Yours sincerely,"
//			   + "\n\n SIT Enquiry System"
//			   + "\n\n Please do not reply. To confirm this is a genuine email sent by the Enquiry System SIT.");
//
//			Transport.send(message);
//
//			logger.info("It have done to reply email to " + enquiry.getSid());
//
//	   } catch (MessagingException e) {
//		   throw new RuntimeException(e);
//	   }
//	}
//	
//	public void forwardTo(Enquiry enquiry) {
//		
//		try {
//			message.setRecipients(Message.RecipientType.TO,
//			   InternetAddress.parse(enquiry.getForwardTo()));
//			message.setSubject("A enquiry message(" + enquiry.getId() + ") from " + enquiry.getSid());
//			message.setText("Hi, to whom may concern,"
//					+ "\n\n A enquiry was forwarded to you from " + auth.getName() + ". Please help to reply."
//					+ "\n\n " + enquiry.getForwardedComment()
//					
//					+ "\n\n The enquiry (refferece No:"+ enquiry.getId() +") is from student " + enquiry.getFirstName() + " (SID: " + enquiry.getSid() + "). All information is listed below: "
//				    + "\n\n Student Id \t\t\t:\t" + enquiry.getSid()
//					+ "\n First Name \t\t\t:\t" + enquiry.getFirstName()
//					+ "\n Email Address \t\t\t:\t" + enquiry.getEmail() + "@sydney.uni.edu.au"
//					+ "\n Post/Undergraduate \t\t:\t" + enquiry.getLevel()
//					+ "\n Enrolled Degree \t\t:\t" + enquiry.getDegree()
//					+ "\n Current Major \t\t\t:\t" + enquiry.getMajor()
//					+ "\n Related to \t\t\t:\t" + enquiry.getRelatedTo()
//					+ "\n Student's Question \t\t:\t" + enquiry.getQuestion()
//					
//				   + "\n\n Yours sincerely,"
//				   + "\n\n SIT Enquiry System"
//				   + "\n\n Please do not reply. To confirm this is a genuine email sent by the Enquiry System SIT.");
//
//			Transport.send(message);
//
//			logger.info("It have done to forwardTo email to " + enquiry.getForwardTo());
//
//	   } catch (MessagingException e) {
//		   throw new RuntimeException(e);
//	   }
//	}
//		
//	private String getStudyLevel(Enquiry enquiry){
//		String authStr = "";
//			
//		
//		if(enquiry.getLevel().contains("Undergraduate - 1st Year")){
//			authStr = "josiah@it.usyd.edu.au";
//		}
//		else if(enquiry.getLevel().contains("Undergraduate - 2st Year")){
//			authStr = "bing.zhou@sydney.edu.au";
//		}
//		else if(enquiry.getLevel().contains("Undergraduate - 3st Year")){
//			authStr = "vchung@it.usyd.edu.au";
//		}
//		else if(enquiry.getLevel().contains("Undergraduate - 4st Year")){
//			authStr = "josiah@it.usyd.edu.au";
//		}
//		else if(enquiry.getLevel().contains("PostGraduate - coursework")){
//			authStr = "pg.sit.usyd@gmail.com";
//		}
//		else {
//			authStr = "enquiry.sit.usyd@gmail.com";
//		}
//		//System.out.println(enquiry.getLevel().contains(""));
//		return authStr;
//	}
}

