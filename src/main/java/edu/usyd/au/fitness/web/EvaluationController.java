package edu.usyd.au.fitness.web;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import edu.usyd.au.fitness.domain.Evaluation;
import edu.usyd.au.fitness.domain.Result;
import edu.usyd.au.fitness.domain.User;
import edu.usyd.au.fitness.service.AwardManager;
import edu.usyd.au.fitness.service.EvaluationManager;
import edu.usyd.au.fitness.service.ResultManager;


@Controller
@SessionAttributes(value="evaluation",types=Evaluation.class)
@RequestMapping(value="/evaluation/**")
public class EvaluationController {
	
	
	
	@Resource(name="evaluationManager")
	private EvaluationManager evaluationManager;
	
	@Resource(name="awardManager")
	private AwardManager awardManager;
	
	@Resource(name="resultManager")
	private ResultManager resultManager;

	
	@RequestMapping(value="/result", method=RequestMethod.POST)
	public ModelAndView addEvaluation(Model model, Evaluation evaluation,
			HttpSession session, HttpServletRequest httpServletRequest) throws ParseException {
		
		User user =  (User)session.getAttribute("user");
		
       // DateFormat dateFormatNeeded = new SimpleDateFormat("dd/MM/yyyy");  
        DateFormat  userDateFormat= new SimpleDateFormat("yyyy-MM-dd");  
        Date date = userDateFormat.parse(httpServletRequest.getParameter("wdate"));  
      //  String convertedDate = dateFormatNeeded.format(date);  
		//date =dateFormatNeeded.parse(convertedDate);
		System.out.println(date.toString());
		evaluation.setUserName(user.getUserName());
		System.out.println(user.getUserName());
		
		evaluation.setDate(date);
		evaluation.setId(user.getId());
		System.out.println(evaluation.getDate().toString());
		this.evaluationManager.addEvaluation(evaluation);
		Result result = new Result();
		result.setUserName(evaluation.getUserName());
		result.setDate(evaluation.getDate());
		result.setScore(evaluation.calculateScore());
		if(result.getScore()>=60)
		{
			result.setLevel("green");
			awardManager.updateAward(user.getUserName(), 5);
		}
		else if(result.getScore()>=40)
		{
			result.setLevel("yellow");
		}
		else
		{
			result.setLevel("red");
		}
		
		
		this.resultManager.addResult(result);
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("result", result);
		
		
		return new ModelAndView("result", "model", myModel);
	}
	
	 public ModelAndView addevaluation(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {

	        String now = (new java.util.Date()).toString();
	        //logger.info("returning hello view with " + now);

	        Map<String, Object> myModel = new HashMap<String, Object>();
	        myModel.put("result", now);
	       // myModel.put("products", this.productManager.getProducts());

	        return new ModelAndView("result", "model", myModel);
	    }
}
