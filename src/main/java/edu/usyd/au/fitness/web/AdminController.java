package edu.usyd.au.fitness.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import edu.usyd.au.fitness.domain.User;
import edu.usyd.au.fitness.domain.recommend.RecommendedActivity;
import edu.usyd.au.fitness.service.CalendarService;
//import edu.usyd.au.fitness.service.LoginInput;

/**
 * Handles requests for the application Admin page.
 */
@Controller
@RequestMapping(value="/admin/**")
public class AdminController {
	
	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);
	
	@Autowired
	private CalendarService calendarService;
	
	/**
	 * Simply selects the Admin view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView calendar(Model model) {
		
		List<RecommendedActivity> acts = calendarService.queryRecommendedActivity();
		
		model.addAttribute("acts", acts);
		
		return new ModelAndView("/admin/calendar", "model", model);
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView save(Model model, @ModelAttribute("recommendedActivity") RecommendedActivity act, HttpServletRequest request) {
	
		calendarService.updateAct(act);
		
		return new ModelAndView("redirect:/admin");
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView add(Model model, @ModelAttribute("recommendedActivity") RecommendedActivity act, HttpServletRequest request) {
	
		calendarService.addAct(act);
		
		return new ModelAndView("redirect:/admin");
	}
}
