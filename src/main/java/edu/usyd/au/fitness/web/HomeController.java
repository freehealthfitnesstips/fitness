package edu.usyd.au.fitness.web;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.crypto.Data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import edu.usyd.au.fitness.domain.Activity;
import edu.usyd.au.fitness.domain.Award;
import edu.usyd.au.fitness.domain.FatSecretFood;
import edu.usyd.au.fitness.domain.Food;
import edu.usyd.au.fitness.domain.UserActivity;
import edu.usyd.au.fitness.domain.Result;
import edu.usyd.au.fitness.domain.UserFood;
import edu.usyd.au.fitness.domain.UserWeight;
import edu.usyd.au.fitness.service.AwardManager;
import edu.usyd.au.fitness.service.CalendarService;
import edu.usyd.au.fitness.service.EmailService;
import edu.usyd.au.fitness.service.FatSecretAPI;
import edu.usyd.au.fitness.service.FatSecretException;
import edu.usyd.au.fitness.service.FitnessService;
import edu.usyd.au.fitness.domain.Weight;
//import edu.usyd.au.fitness.service.LoginInput;
import edu.usyd.au.fitness.service.LoginService;
import edu.usyd.au.fitness.service.AccountService;
import edu.usyd.au.fitness.service.PlannerService;
import edu.usyd.au.fitness.service.ReminderService;
import edu.usyd.au.fitness.service.ResultManager;
import edu.usyd.au.fitness.domain.User;
import edu.usyd.au.fitness.domain.Planner;
import edu.usyd.au.fitness.domain.Reminder; 

/**
 * Handles requests for the application home page.
 */
@Controller
@SessionAttributes(value="user",types=User.class)
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
//	private User user;
	
	//Hill's part
	
	@Resource(name="resultManager")
	private ResultManager resultManager;
	@Resource(name="awardManager")
	private AwardManager awardManager;
	
	@RequestMapping(value = "/evaluation", method = RequestMethod.GET)
	public String evaluation(Model model) {
		
		return "evaluation";
	}
	
	@RequestMapping(value = "/history", method = RequestMethod.GET)
	public ModelAndView history(Model model) {
		
		List<Result> results = resultManager.getResults();
		model.addAttribute("results", results);
		return new ModelAndView("history", "model", model);
	}
	
	@RequestMapping(value = "/rank", method = RequestMethod.GET)
	public ModelAndView rank(Model model) {
		
		
		List<Award> awards = awardManager.getAwards();
		model.addAttribute("awards", awards);
		//model.addAttribute("currentUser", awardManager.getAwardByUser("lhe"));
		return new ModelAndView("rank", "model", model);
	}
	
	@Autowired
    private FitnessService fitnessService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private PlannerService plannerService;
	@Autowired
	private ReminderService reminderService;
	@Autowired
	private CalendarService calendarService;
//	@Autowired
	private EmailService emailService;	
	
//	@Autowired
//	private LoginInput loginInput;
	@Autowired
	private LoginService loginService;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {
		
		return "welcome";
	}

	
	@RequestMapping(value = "/food", method = RequestMethod.GET)
	public ModelAndView food(Model model,HttpSession session) {
		User user =  (User)session.getAttribute("user");
		//Date cdata=new Date();
		//SimpleDateFormat fdata=new SimpleDateFormat("yyyy-MM-dd").format( new Date());
//		SimpleDateFormat mdata=new SimpleDateFormat("mm");
//		SimpleDateFormat ddata=new SimpleDateFormat("dd");
		String ydateStr = new SimpleDateFormat("yyyy-MM-dd").format( new Date());
		List<UserFood> userfood=fitnessService.queryUserFoodByUserIdandDate(user.getId(),ydateStr);
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		myModel.put("userfoodInfo", userfood);
		//myModel.put("foodInfo", foods);
		myModel.put("isSelect", false);

        return new ModelAndView("food", "model", myModel);
		
	}
	
	@RequestMapping(value = "/activity", method = RequestMethod.GET)
	public ModelAndView activity(Model model,HttpSession session) {	
		User user =  (User)session.getAttribute("user");
		String ydateStr = new SimpleDateFormat("yyyy-MM-dd").format( new Date());
		List<UserActivity> useractivity=fitnessService.queryUserActivityByUserIdandDate(user.getId(),ydateStr);
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		myModel.put("useractivityInfo", useractivity);
		//myModel.put("foodInfo", foods);
		myModel.put("isSelect", false);

		return new ModelAndView("activity", "model", myModel);
	}
	
///////////////////////Eric's part////////////////////////////	
	
//	@RequestMapping(value = "/login", method = RequestMethod.POST)
//	public ModelAndView login(Model model, @ModelAttribute("login") User user) {
//		
//		
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("user", user);
//		
//		return new ModelAndView("home", "model", myModel);
//	}
	
	@RequestMapping(value = "/homepage", method = RequestMethod.GET)
	public ModelAndView homepage(Model model, User user) {
		
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("user", user);
		
		model.addAttribute("user", user);
		
//		return new ModelAndView("home", "model", myModel);
		return new ModelAndView("home");
		
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(Model model) {
		
		return "register";
	}
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.GET)
	public String changePassword(Model model) {
		
		return "changePassword";
	}	
	
	
//	@RequestMapping(value = "/planner", method = RequestMethod.GET)
//	public ModelAndView planner(Model model) {
//		
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("user", user);
//		
//		return new ModelAndView("planner", "model", myModel);
//		
//	}

	@RequestMapping(value = "/planner", method = RequestMethod.GET)
	public ModelAndView planner(Model model) {
		
			return new ModelAndView("planner");

	}
	
	@RequestMapping(value = "/reminder", method = RequestMethod.GET)
	public ModelAndView reminder(Model model) {
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("user", user);
		
		return new ModelAndView("reminder");
	}
	
//	@RequestMapping(value = "/registerConfirm", method = RequestMethod.POST)
//	public ModelAndView registerConfirm(Model model, @ModelAttribute("register") User user) 
////	public ModelAndView registerConfirm(Model model,  User user) 
//	{
//		
////		model.addAttribute(user);
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("user", user);
//		return new ModelAndView("confirmAccount", "model", myModel);
//		
//		
//		
////		Map<String, Object> myModel = new HashMap<String, Object>();
////		myModel.put("user", user);
////		
////		return new ModelAndView("confirmAccount", "model", myModel);
//	}
	
	@RequestMapping(value = "/accountConfirm", method = RequestMethod.POST)
	public ModelAndView accountConfirm(Model model, User user) 
	{
		String result = accountService.checkUserInput(user);
		
		if (result.equals("Same"))
		{
//			model.addAttribute("user", user);
			return new ModelAndView("confirmAccount");
			
		}
		else
		{
			Map<String, Object> myModel = new HashMap<String, Object>();
			myModel.put("error", result);
			return new ModelAndView("register", "model", myModel);
		}
			
		
		
//		model.addAttribute("user", user);
//		return new ModelAndView("confirmAccount");
		
		
		
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("user", user);
//		
//		return new ModelAndView("confirmAccount", "model", myModel);
	}	
	
	
	
	
	
	
//	@RequestMapping(value = "/editAccount", method = RequestMethod.POST)
//	public ModelAndView editAccount(Model model, @ModelAttribute("editAccount") User user)
//	{
//		
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("user", user);
//		
//		return new ModelAndView("editAccount", "model", myModel);
//		
//	}
	
	@RequestMapping(value = "/editAccount", method = RequestMethod.POST)
	public ModelAndView editAccount(Model model, User user)
	{
		String result = accountService.checkUserInput(user);
		
		if (result.equals("Same"))
		{
			return new ModelAndView("confirmAccount");
			
		}
		else
		{
//			Map<String, Object> myModel = new HashMap<String, Object>();
//			myModel.put("error", result);
			return new ModelAndView("editAccount");
		}
		
		
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("user", user);
		
//		return new ModelAndView("editAccount");
		
	}
	
	@RequestMapping(value = "/accountModifiedConfirm", method = RequestMethod.POST)
	public ModelAndView accountModifiedConfirm(Model model, User user) 
	{
		String result = accountService.checkUserInput(user);
		
		if (result.equals("Same"))
		{
//			model.addAttribute("user", user);
			return new ModelAndView("confirmAccount");
			
		}
		else
		{
			Map<String, Object> myModel = new HashMap<String, Object>();
			myModel.put("error", result);
			return new ModelAndView("editAccount", "model", myModel);
		}
			
		
		
//		model.addAttribute("user", user);
//		return new ModelAndView("confirmAccount");
		
		
		
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("user", user);
//		
//		return new ModelAndView("confirmAccount", "model", myModel);
	}	
		
	
	
	
	
	
	
	
	
	
	
	
	
//	@RequestMapping(value = "/saveAccount", method = RequestMethod.POST)
//	public ModelAndView saveAccount(Model model, @ModelAttribute("submitAccount") User user) {
//		
//		this.user = user;
//		registerService.addAccount(user);
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("user", user);
//		
//		return new ModelAndView("registerSuccess", "model", myModel);
//	}
	
	@RequestMapping(value = "/saveAccount", method = RequestMethod.POST)
	public ModelAndView saveAccount(Model model, User user) {
		
//		this.user = user;
		emailService = new EmailService();
//		emailService.accountNotification(user);
		accountService.addAccount(user);
		model.addAttribute("user", user);
		
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("user", user);
		
		return new ModelAndView("registerSuccess");
	}
	
//	@RequestMapping(value = "/plannerConfirm", method = RequestMethod.POST)
//	public ModelAndView plannerConfirm(Model model, @ModelAttribute("planner") Planner planner) {
//		
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("planner", planner);
//		
//		return new ModelAndView("confirmPlanner", "model", myModel);
//	}
	
	@RequestMapping(value = "/plannerConfirm", method = RequestMethod.POST)
	public ModelAndView plannerConfirm(Model model, @ModelAttribute("planner") Planner planner) {
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("planner", planner);
		
		return new ModelAndView("confirmPlanner", "model", myModel);
	}	
	
	@RequestMapping(value = "/editPlanner", method = RequestMethod.POST)
	public ModelAndView editPlanner(Model model, @ModelAttribute("editPlanner") Planner planner)
	{
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("planner", planner);
		
		return new ModelAndView("editPlanner", "model", myModel);
		
	}
	
	@RequestMapping(value = "/savePlanner", method = RequestMethod.POST)
	public ModelAndView savePlanner(Model model, Planner planner) {
		
		String result = plannerService.checkPlanner(planner);
		Planner temp;
		if (result.equals("exist"))
		{
			
			temp = plannerService.updatePlanner(planner);
//			accountService.updatePlannerIntoAccount(planner);
//			planner.setId(plannerService.getExactPlanner().getId());
			
//			temp = plannerService.updatePlanner(planner);
		}
		else
		{
		
			temp = plannerService.addPlanner(planner);
			accountService.addPlannerIntoAccount(planner);
		}
		

//		plannerService.calculation(planner);
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("planner", temp);
		
		return new ModelAndView("plannerSuccess", "model", myModel);
	}
	
	@RequestMapping(value = "/setReminder", method = RequestMethod.POST)
	public ModelAndView setReminder(Model model, @ModelAttribute("reminder") Reminder reminder) {
		
		reminderService.setReminder(reminder);
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("reminder", reminder);
		
		return new ModelAndView("reminderSuccess", "model", myModel);
		
	}
	
//	@RequestMapping(value = "/setLoginInput", method = RequestMethod.POST)
//	public ModelAndView setLoginInput(Model model, @ModelAttribute("loginInput") User input) {
//		
//		String result = loginService.validator(input);
//		
////		Map<String, Object> myModel = new HashMap<String, Object>();
////		myModel.put("input", input);
//		if (result.equals("Matched"))
//		{
//			return homepage(loginService.getExactUser());
//		}
//		else
//		{
//			Map<String, Object> myModel = new HashMap<String, Object>();
//			myModel.put("error", result);
//				
//			return new ModelAndView("welcome", "model", myModel);
//			
//		}
	
	@RequestMapping(value = "/setLoginInput", method = RequestMethod.POST)
	public ModelAndView setLoginInput(Model model, User input) {
		
		String result = loginService.validator(input);
		
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("input", input);
		if (result.equals("Matched"))
		{
			return homepage(model, loginService.getExactUser());
		}
		else
		{
			Map<String, Object> myModel = new HashMap<String, Object>();
			myModel.put("error", result);
				
			return new ModelAndView("welcome", "model", myModel);
			
		}
		
	}
	
//	@RequestMapping(value = "/updateAccount", method = RequestMethod.POST)
//	public ModelAndView updateAccount(Model model, @ModelAttribute("updateAccount") User user) {
//		
//		Planner temp = plannerService.addPlanner(user);
////		plannerService.calculation(planner);
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("planner", temp);
//		
//		return new ModelAndView("plannerSuccess", "model", myModel);
//	}
	
	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public String update(Model model) {
		
		return "updateAccount";
	}
	
	
	
	
	
	@RequestMapping(value = "/updateAccount", method = RequestMethod.POST)
	public ModelAndView updateAccount(Model model, User user) {
		
		String result = accountService.checkUserInput(user);
		
		if (result.equals("Same"))
		{
			emailService = new EmailService();
//			emailService.updateNotification(user);
			accountService.updateAccount(user);
			return updateAccountSuccess(model, user);
			
		}
		else
		{
			Map<String, Object> myModel = new HashMap<String, Object>();
			myModel.put("error", result);
			return new ModelAndView("updateAccount", "model", myModel);
			
		}
		
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("planner", temp);
//		
//		return new ModelAndView("plannerSuccess", "model", myModel);
	}
	
	public ModelAndView updateAccountSuccess(Model model, User user) {
		
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("user", user);
		
		model.addAttribute("user", user);
		
//		return new ModelAndView("home", "model", myModel);
		return new ModelAndView("updateAccountSuccess");
		
	}
	
	@RequestMapping(value = "/reset", method = RequestMethod.GET)
	public String reset(Model model) {
		
		return "reset";
	}
	
	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
	public ModelAndView resetPassword(Model model, User input) {
		
		String result = loginService.searchId(input);
		
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("input", input);
		if (!result.equals("Email Required!") && !result.equals("Login ID not exist!"))
		{
			input.setPassword(result);
			return sendPassword(model, input);
		}
		else
		{
			Map<String, Object> myModel = new HashMap<String, Object>();
			myModel.put("error", result);
				
			return new ModelAndView("reset", "model", myModel);
			
		}
		
	}
	
	@RequestMapping(value = "/sendPassword", method = RequestMethod.GET)
	public ModelAndView sendPassword(Model model, User input) {
		
//		Map<String, Object> myModel = new HashMap<String, Object>();
//		myModel.put("user", user);
		
//		model.addAttribute("user", input);
		emailService.resetNotification(input);
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("user", input);
		return new ModelAndView("passwordSent", "model", myModel);
//		return new ModelAndView("home");
		
	}
	
	
	
	
	
///////////////////////Eric's part////////////////////////////
	
	@RequestMapping(value = "/calendar", method = RequestMethod.GET)
	public ModelAndView calendar(Model model, HttpSession session) {
		User user =  (User)session.getAttribute("user");
		String events = calendarService.getJsonOfActivities(user);
		model.addAttribute("events", events);
		return new ModelAndView("calendar", "model", model);
	}
	
	@RequestMapping(value = "/saveFood", method = RequestMethod.POST)
	public ModelAndView saveFood(Model model, @ModelAttribute("food") Food food, HttpSession session) {
		
		//food.setCals(food.getAmount()*food.getCals());
		//fitnessService.addFood(food);
		User user =  (User)session.getAttribute("user");
		//User user2 = fitnessService.queryUserById(2);
		//Food food2 = fitnessService.queryFoodById(1);
		//List<UserFood> userfood2=fitnessService.queryUserFood(2);
		//Set<UserFood> userfood2=user.getUserFoodList();

		//List<User> user2=fitnessService.queryUserByEmail("123");
		Food foods=new Food();
		List<Food> foodDb=fitnessService.queryFoodByfoodNmae(food.getFoodName());
		if(foodDb.size()>0)
		{
			foods=foodDb.get(0);
		}
		UserFood userfood= new UserFood();
		userfood.setFood(foods);
		userfood.setUser(user);
		userfood.setAmount(food.getAmount());
		fitnessService.addUserFood(userfood);

		String ydateStr = new SimpleDateFormat("yyyy-MM-dd").format( new Date());
		List<UserFood> userfoods=fitnessService.queryUserFoodByUserIdandDate(user.getId(),ydateStr);
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		myModel.put("userfoodInfo", userfoods);
		myModel.put("isAddedSuc", true);
		return new ModelAndView("food", "model", myModel);
	}

	
	@RequestMapping(value = "/saveActivity", method = RequestMethod.POST)
	public ModelAndView saveActivity(Model model, @ModelAttribute("activity") Activity activity, HttpSession session) {
		User user =  (User)session.getAttribute("user");
		Activity activities=new Activity();
		List<Activity> activityDb=fitnessService.queryActivitiesByName(activity.getName());
		if(activityDb.size()>0)
		{
			activities=activityDb.get(0);
		}
		UserActivity userActivity=new UserActivity();
		userActivity.setUser(user);
		userActivity.setActivity(activities);
		userActivity.setDuration(activity.getDuration());
		fitnessService.addUserActivity(userActivity);
		
		String ydateStr = new SimpleDateFormat("yyyy-MM-dd").format( new Date());
		List<UserActivity> useractivity=fitnessService.queryUserActivityByUserIdandDate(user.getId(),ydateStr);
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		myModel.put("useractivityInfo", useractivity);
		myModel.put("isAddedSuc", true);
		return new ModelAndView("activity", "model", myModel);
	}
	
	
	
	@RequestMapping(value = "/foodRecords", method = RequestMethod.GET)
	public ModelAndView foodRecords(Model model) {
		
		List<Food> foods = fitnessService.queryFoods();
		model.addAttribute("foods", foods);
		
		return new ModelAndView("foodRecords", "model", model);
	}
	
	@RequestMapping(value = "/activityRecords", method = RequestMethod.GET)
	public ModelAndView activityRecords(Model model) {
		
		List<Activity> activities = fitnessService.queryActivities();
		model.addAttribute("activities", activities);
		
		return new ModelAndView("activityRecords", "model", model);
	}
	
	
	@RequestMapping(value = "/weight", method = RequestMethod.GET)
	public String Weight(Model model) {
		
		return "weight";
	}
	
	@RequestMapping(value="/charts", method=RequestMethod.GET)
	public ModelAndView charts(HttpSession session) {
		Map<String, Object> myModel = new HashMap<String, Object>();
		User user =  (User)session.getAttribute("user");
		List<UserWeight> userweight=fitnessService.queryUserWeightByUserId(user.getId());
		List<Weight> weights=new ArrayList<Weight>();
		List qw=new ArrayList();
		for(int i=0;i<userweight.size();i++)
		{
			Weight weight=fitnessService.queryWeightById(userweight.get(i).getWeight().getId());			
			//List<String> list= new ArrayList<String>(); 
			weights.add(weight);
//			SimpleDateFormat ydata=new SimpleDateFormat("yyyy-mm-d");
//			SimpleDateFormat mdata=new SimpleDateFormat("mm");
//			SimpleDateFormat ddata=new SimpleDateFormat("dd");
//			String ydateStr = ydata.format(weight.getCreatedDate());
//			int ab=Integer.parseInt(ydateStr);
//			qw.add(ab);
//			String mdateStr = mdata.format(weight.getCreatedDate());
//			String ddateStr = ddata.format(weight.getCreatedDate());
//			String a="w";
		}
		Planner planner=user.getPlanner();
		myModel.put("planner", planner);
		String ydateStr = new SimpleDateFormat("yyyy-MM-dd").format( new Date());
		List<UserFood> userfood30=fitnessService.queryUserFoodByUserIdandDate(user.getId(),"2013-10-30");
		List<UserFood> userfood29=fitnessService.queryUserFoodByUserIdandDate(user.getId(),"2013-10-29");
		List<UserFood> userfood28=fitnessService.queryUserFoodByUserIdandDate(user.getId(),"2013-10-28");
		List<UserFood> userfood27=fitnessService.queryUserFoodByUserIdandDate(user.getId(),"2013-10-27");
		List<UserFood> userfood26=fitnessService.queryUserFoodByUserIdandDate(user.getId(),"2013-10-26");
		List<UserFood> userfood25=fitnessService.queryUserFoodByUserIdandDate(user.getId(),"2013-10-25");
		myModel.put("userfoodInfo30", userfood30);
		myModel.put("userfoodInfo29", userfood29);
		myModel.put("userfoodInfo28", userfood28);
		myModel.put("userfoodInfo27", userfood27);
		myModel.put("userfoodInfo26", userfood26);
		myModel.put("userfoodInfo25", userfood25);
		
		List<UserActivity> useractivity30=fitnessService.queryUserActivityByUserIdandDate(user.getId(),"2013-10-30");
		List<UserActivity> useractivity29=fitnessService.queryUserActivityByUserIdandDate(user.getId(),"2013-10-29");
		List<UserActivity> useractivity28=fitnessService.queryUserActivityByUserIdandDate(user.getId(),"2013-10-28");
		List<UserActivity> useractivity27=fitnessService.queryUserActivityByUserIdandDate(user.getId(),"2013-10-27");
		List<UserActivity> useractivity26=fitnessService.queryUserActivityByUserIdandDate(user.getId(),"2013-10-26");
		List<UserActivity> useractivity25=fitnessService.queryUserActivityByUserIdandDate(user.getId(),"2013-10-25");
		myModel.put("useractivityInfo30", useractivity30);
		myModel.put("useractivityInfo29", useractivity29);
		myModel.put("useractivityInfo28", useractivity28);
		myModel.put("useractivityInfo27", useractivity27);
		myModel.put("useractivityInfo26", useractivity26);
		myModel.put("useractivityInfo25", useractivity25);
		
		myModel.put("weight", weights);
        return new ModelAndView("charts", "model", myModel);
      
	}
		
	@RequestMapping(value="/foodSearch", method=RequestMethod.POST)
	public ModelAndView foodSearch(HttpServletRequest request, HttpServletRequest session) {
		User user =  (User)session.getAttribute("user");
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		FatSecretFood food=null;
		String fn=null;
		if(!"".equals(request.getParameter("foodName")) && request.getParameter("foodName") != null){
			 fn = request.getParameter("foodName");	
			 FatSecretAPI api= new FatSecretAPI("428a4f3108ef464a8e6f6f535d5752b3","51063cb2c32c47f3aaff22336df4557f");
			 try 
				{
					 food = api.FoodGet(fn);
					 myModel.put("foodinf", food);
					 myModel.put("isSelect", true);
				} 
				catch (FatSecretException e) 
				{
					myModel.put("nofind", "Sorry, we do not have any results.")	;
				}
		}
		else {
			myModel.put("nofind", "Input Food Name.");
		}
		
		//String ydateStr = new SimpleDateFormat("yyyy-MM-dd").format( new Date());
		//List<UserFood> userfood=fitnessService.queryUserFoodByUserIdandDate(user.getId(),ydateStr);
		
		
		//myModel.put("userfoodInfo", userfood);
		
        return new ModelAndView("food", "model", myModel);
      
	}
	
	@RequestMapping(value = "/weightLog", method = RequestMethod.POST)
	public ModelAndView weightLog(Model model, HttpSession session, HttpServletRequest request) {
		double cw=0;
		if(!"".equals(request.getParameter("currentweight")) && request.getParameter("currentweight") != null){
			cw = Double.parseDouble(request.getParameter("currentweight"));	
		}
		Weight weight=new Weight();
		weight.setCurrentWeight(cw);
		fitnessService.addWeight(weight);
		User user =  (User)session.getAttribute("user");		
		UserWeight userweight= new UserWeight();
		userweight.setWeight(weight);
		userweight.setUser(user);
		fitnessService.addUserWeight(userweight);
		
		return new ModelAndView("weight", "isAddedSuc", true);
	}

	@RequestMapping(value="/activitySearch", method=RequestMethod.POST)
	public ModelAndView activitySearch(HttpServletRequest request, HttpSession session) {
		User user =  (User)session.getAttribute("user");
		String ydateStr = new SimpleDateFormat("yyyy-MM-dd").format( new Date());
		List<UserActivity> useractivity=fitnessService.queryUserActivityByUserIdandDate(user.getId(),ydateStr);
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		myModel.put("useractivityInfo", useractivity);
		
		//Map<String, Object> myModel = new HashMap<String, Object>();
		List<Activity> activities=new ArrayList<Activity>();
		if(!"".equals(request.getParameter("activityName")) && request.getParameter("activityName") != null){
			activities=fitnessService.queryActivitiesByName(request.getParameter("activityName"));
			if(activities.size()>0)
			{
				myModel.put("activitiesinf", activities.get(0));
				myModel.put("isSelect", true);
			}
			else {
				myModel.put("nofind", "Sorry, we do not have any results.")	;
			}
		}
		else {
			myModel.put("nofind", "Input a activity!")	;
		}
		//myModel.put("activitiesinf", activities);
		
		
        return new ModelAndView("activity", "model", myModel);
      
	}
}
