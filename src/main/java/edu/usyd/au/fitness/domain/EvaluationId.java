package edu.usyd.au.fitness.domain;

import java.io.Serializable;
import java.util.Date;

public class EvaluationId implements Serializable {

	private String userName;
	private Date date;
}
