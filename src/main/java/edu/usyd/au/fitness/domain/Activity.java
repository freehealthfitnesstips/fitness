package edu.usyd.au.fitness.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="ACTIVITY")
@SequenceGenerator(name="seq_actvity", initialValue=100001, allocationSize=100,sequenceName="seq_actvity")
public class Activity implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;
		

	@Column(name="NAME", length = 50)
    private String name;
	
	@Column(name="DURATION", length = 20)
	private double duration;
	
	@Column(name="CALS", length = 50)
    private double cals;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATEDDATE")
    private Date createdDate;

	@OneToMany(mappedBy="activity",cascade=CascadeType.ALL)
	private Set<UserActivity> userActivityList;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

	public double getCals() {
		return cals;
	}

	public void setCals(double cals) {
		this.cals = cals;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Set<UserActivity> getUserActivityList() {
		return userActivityList;
	}

	public void setUserActivityList(Set<UserActivity> userActivityList) {
		this.userActivityList = userActivityList;
	}

	
}
