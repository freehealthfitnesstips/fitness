package edu.usyd.au.fitness.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
//import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NaturalId;


@Entity 
@IdClass(EvaluationId.class)
@Table(name="EVALUATION")
@SequenceGenerator(name="seq_user", initialValue=100001, allocationSize=100,sequenceName="seq_user")

public class Evaluation implements Serializable {


	
	@GeneratedValue
	@Column(name="Id")
	private int id;
	
	@Id
	@Column(name="USER_NAME", length = 20)
	private String userName;
	
	@Id
	@Column(name="DATE")
	private Date date;

	@Column(name="PLAN_SITUATION", length = 20)
	private String planRate;

	@Column(name="EXERCISE_TYPE", length = 20)
	private String exerciseType;

	@Column(name="EXERCISE_HOUR", length = 20)
	private int exerciseHour;

	@Column(name="CALORIES_INTAKE", length = 20)
	private int caloriesIntake;

	@Column(name="GOAL_REACH", length = 20)
	private String goalReach;

	@Column(name="SITTING_TIME", length = 20)
	private int sittingTime;

	@Column(name="SLEEPING_TIME", length = 20)
	private int sleepingTime;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
		//System.out.println(this.date.toString());
	}

	
	public String getPlanRate() {
		return planRate;
	}
	public void setPlanRate(String planRate) {
		this.planRate = planRate;
	}

	
	public String getExerciseType() {
		return exerciseType;
	}
	public void setExerciseType(String exerciseType) {
		this.exerciseType = exerciseType;
	}
	
	
	public int getExerciseHour() {
		return exerciseHour;
	}
	public void setExerciseHour(int exerciseHour) {
		this.exerciseHour = exerciseHour;
	}
	
	
	public int getCaloriesIntake() {
		return caloriesIntake;
	}
	public void setCaloriesIntake(int caloriesIntake) {
		this.caloriesIntake = caloriesIntake;
	}
	
	
	public String getGoalReach() {
		return goalReach;
	}
	public void setGoalReach(String goalReach) {
		this.goalReach = goalReach;
	}

	
	public int getSittingTime() {
		return sittingTime;
	}
	public void setSittingTime(int sittingTime) {
		this.sittingTime = sittingTime;
	}

	
	public int getSleepingTime() {
		return sleepingTime;
	}
	public void setSleepingTime(int sleepingTime) {
		this.sleepingTime = sleepingTime;
	}
	
	public int calculateScore() {
		
		int base=900;
		int temp = 0;
		int caloriesScore=0;
		int healthScore=0;
		double pRate=50;
		double bias=0;
		if(exerciseType.equalsIgnoreCase("None"))
		{
			temp=0;
		}
		else if(exerciseType.equalsIgnoreCase("Light"))
		{
			temp=100;
		}
		else if(exerciseType.equalsIgnoreCase("Normal"))
		{
			temp=200;
		}
		else if(exerciseType.equalsIgnoreCase("Vigorous"))
		{
			temp=400;
		}
		int calConsume=base+temp*exerciseHour;
		
		if(planRate.equalsIgnoreCase("90%-100%"))
		{
			pRate=95;
		}
		else if(planRate.equalsIgnoreCase("80%-89%"))
		{
			pRate=85;
		}
		else if(planRate.equalsIgnoreCase("70%-79%"))
		{
			pRate=75;
		}
		else if(planRate.equalsIgnoreCase("60%-69%"))
		{
			pRate=65;
		}
		else if(planRate.equalsIgnoreCase("below 60%"))
		{
			pRate=45;
		}
		
		if(calConsume/caloriesIntake>=1)
		{
			bias=1.0;
		}
		else
		{
			bias=((double)calConsume)/caloriesIntake;
		}
		
		caloriesScore=(int) (25*pRate/100+25*bias);
		if(goalReach.equalsIgnoreCase("yes"))
		{
			temp=1;
		}
		else
		{
			temp=0;
		}
		
		if(sleepingTime/sittingTime>=1)
		{
			bias=1.0;
		}
		else
		{
			bias=((double)sleepingTime)/sittingTime*0.8;
		}
		System.out.println(caloriesScore+"rrr"+bias);
		System.out.println(calConsume+"cccc"+caloriesIntake);
		healthScore=(int) (10.0*temp+bias*40);
		return healthScore+caloriesScore;
	}
}
