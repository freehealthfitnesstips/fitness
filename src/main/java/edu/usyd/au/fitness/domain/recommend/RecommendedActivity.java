package edu.usyd.au.fitness.domain.recommend;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="RECOMMENDED_ACTIVITY")
@SequenceGenerator(name="SEQ_RECOMMENDED_ACTIVITY", initialValue=100001, allocationSize=100,sequenceName="SEQ_RECOMMENDED_ACTIVITY")
public class RecommendedActivity implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="ID")
	private int id;

	@Column(name="USERID")
	private int user_id;
	
	@Column(name="NAME", length = 70)
    private String title;
	
	@Column(name="WARMUP", length = 70)
    private String warmUp;
	
	@Column(name="EXERCISE", length = 70)
    private String exercise;
	
	@Column(name="COOLDOWN", length = 70)
    private String coolDown;
	
	@Column(name="TOTALTIME", length = 70)
    private String totalTime;
	
	@Column(name="SEQUENCY", length = 70)
    private int sequency;
	
	@Column(name="RANGE_OF_DATE", length = 70)
    private int rangeOfDate;
	
	@Column(name="ENABLE")
    private boolean enable;
	
	private String start;
	
	private String end;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWarmUp() {
		return warmUp;
	}

	public void setWarmUp(String warmUp) {
		this.warmUp = warmUp;
	}

	public String getExercise() {
		return exercise;
	}

	public void setExercise(String exercise) {
		this.exercise = exercise;
	}

	public String getCoolDown() {
		return coolDown;
	}

	public void setCoolDown(String coolDown) {
		this.coolDown = coolDown;
	}

	public String getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(String totalTime) {
		this.totalTime = totalTime;
	}

	public int getSequency() {
		return sequency;
	}

	public void setSequency(int sequency) {
		this.sequency = sequency;
	}

	public int getRangeOfDate() {
		return rangeOfDate;
	}

	public void setRangeOfDate(int rangeOfDate) {
		this.rangeOfDate = rangeOfDate;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	
}
