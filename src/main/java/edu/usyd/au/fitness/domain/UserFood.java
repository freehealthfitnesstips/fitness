package edu.usyd.au.fitness.domain;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;


@Entity
@Table(name="USERFOOD")
@SequenceGenerator(name="seq_userfood", initialValue=100001, allocationSize=100,sequenceName="seq_userfood")
public class UserFood implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;

	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "user_Id")//, unique = true)
	private User user;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "food_Id")//, unique = true)
	private Food food;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATEDDATE")
    private Date createdDate;
	
	@Column(name="AMOUNT", length = 20)
    private double amount;
	
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Food getFood() {
		return food;
	}

	public void setFood(Food food) {
		this.food = food;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}
