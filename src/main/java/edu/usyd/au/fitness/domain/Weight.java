package edu.usyd.au.fitness.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="WEIGHT")
@SequenceGenerator(name="seq_weight", initialValue=100001, allocationSize=100,sequenceName="seq_weight")
public class Weight implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="Id")
	private int id;

	@Column(name="CURRENTWEIGHT", length = 20)
    private double currentWeight;
	

	@Temporal(TemporalType.DATE)
	@Column(name="CREATEDDATE")
    private Date createdDate;
	
	@OneToMany(mappedBy="weight",cascade=CascadeType.ALL)
	private Set<UserWeight> userWeightList;	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getCurrentWeight() {
		return currentWeight;
	}

	public void setCurrentWeight(double currentWeight) {
		this.currentWeight = currentWeight;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Set<UserWeight> getUserWeightList() {
		return userWeightList;
	}

	public void setUserWeightList(Set<UserWeight> userWeightList) {
		this.userWeightList = userWeightList;
	}
}