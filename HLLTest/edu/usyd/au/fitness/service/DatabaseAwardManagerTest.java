package edu.usyd.au.fitness.service;

import junit.framework.TestCase;



import edu.usyd.au.fitness.domain.Award;

public class DatabaseAwardManagerTest extends TestCase {

	private DatabaseAwardManager dam;
	private int size=2;
	protected void setUp() throws Exception {
		dam = new DatabaseAwardManager();

		Award award= new Award();

		award.setId(0);
		award.setUserName("test1");
		award.setCredit(50);
		//dam.addAward(award);

		award.setId(1);
		award.setUserName("test2");
		award.setCredit(100);
		//dam.addAward(award);
	}

	public void testGetAwardsWithoudAward()
	{
		dam = new DatabaseAwardManager();
		assertNull(null);
	}

	public void testGetAwardsAndgetAwardByUser()
	{
		assertNull(dam.getAwards());

		assertEquals(2, dam.getAwards().size());
		Award award =dam.getAwards().get(0);

		assertEquals(award.getUserName(), "test1");
		assertEquals(award,dam.getAwardByUser("test1") );
		assertEquals(award.getCredit(), 50);
		
		award =dam.getAwards().get(1);

		assertEquals(award.getUserName(), "test2");
		assertEquals(award,dam.getAwardByUser("test2") );
		assertEquals(award.getCredit(), 100);
	}
	public void testUpdateAwards()
	{
		
		Award award =dam.getAwards().get(0);
		assertEquals(award.getCredit(), 50);
		dam.updateAward("test1", 25);
		assertEquals(award.getCredit(), 75);
		assertEquals(award.getBronze(), 3);
		
		award =dam.getAwards().get(1);
		
		assertEquals(award.getCredit(), 100);
		dam.updateAward("test1", 25);
		assertEquals(award.getCredit(), 125);
		assertEquals(award.getSilver(), 1);
		
		assertEquals(2, dam.getAwards().size());
		award =dam.getAwards().get(1);

		assertEquals(award.getUserName(), "test2");
		assertEquals(award,dam.getAwardByUser("test2") );
		assertEquals(award.getCredit(), 100);
	}
}
